﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Danishevskii.Chat.Hubs;
using Microsoft.AspNet.SignalR;

namespace Danishevskii.Chat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NameInput()
        {
            if(TempData["Error"]!=null)
                ViewBag.Message = TempData["Error"].ToString();
            return View();
        }

        public ActionResult ChatBlock(string name)
        {
            if (name == string.Empty)
            {
                TempData["Error"] = "Имя пусто!";
                return RedirectToAction("NameInput");
            }
            return View(null,null, name);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}