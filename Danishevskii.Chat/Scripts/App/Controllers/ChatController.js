﻿var ChatController = function ($scope, $http, $location) {

    $scope.findWithAttr = function (array, attr, value) {
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
    };

    /*первичная инициализация*/
    $scope.message = "";
    $scope.chatlist = [];
    $scope.userList = [];


    var chat = $.connection.сhatHub;

    /*Инициализируем имя юзера из модели*/
    $scope.init = function (name)
    {
        $scope.name = name;
    }
    /*Отправляем сообщение*/
    $scope.sendMessage = function ()
    {
        chat.server.send($scope.name, $scope.message);
        $scope.message = "";
    }
    /*Подключение успешно произведено, инициализируем список текущих пользователей чата*/
    chat.client.onConnected = function (users) {
        $scope.userList = users;
        $scope.$apply();
    }
    /*Добавлен новый пользователь чата*/
    chat.client.onNewUserConnected = function (user) {
        $scope.userList.push(user);
        $scope.$apply();
    }
    /*Пришло новое сообщение*/
    chat.client.addMessage = function (chatMessage) {
        $scope.chatlist.push(chatMessage);
        $scope.$apply();
    }
    /*Пользователь покинул чат*/
    chat.client.onUserDisconnected = function (user) {

        var index = $scope.findWithAttr($scope.userList, 'ConnectionId', user.ConnectionId); 
        $scope.userList.splice(index, 1);

        $scope.$apply();
    }


    $.connection.hub.start().done(function () {
        /*присоединяемся к чату*/
        chat.server.connect($scope.name);
    })
    .fail(function (e) {
        alert('There was an error');
        console.error(e);
    });
}