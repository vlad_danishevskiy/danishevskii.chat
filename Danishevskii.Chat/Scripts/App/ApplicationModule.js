﻿var app = angular.module("app", []);

app.controller('ChatController', ['$scope', '$http', '$location', ChatController]);
app.controller('InputNameController', ['$scope', InputNameController]);