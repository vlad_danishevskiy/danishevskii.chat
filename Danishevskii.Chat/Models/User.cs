﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danishevskii.Chat.Models
{
    public class User
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }
}