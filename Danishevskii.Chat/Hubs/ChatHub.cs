﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Danishevskii.Chat.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Danishevskii.Chat.Hubs
{
    [HubName("сhatHub")]
    public class ChatHub : Hub
    {
        public ChatHub()
        {
            
        }


        public static List<User> Users = new List<User>();

        //Отправка сообщений
        public void Send(string name, string message)
        {
            Clients.All.addMessage(new ChatMessage() { UserName = name, Message = message });
        }

        //Подключение пользователя
        public void Connect(string userName)
        {
            var connId = Context.ConnectionId;
            if (Users.All(c => c.ConnectionId != connId))
            {
                var newUser = new User() {ConnectionId = connId, Name = userName};
                Users.Add(newUser);

                //Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(Users);
                //Посылаем всем пользователям сообщние о новом пользователе
                Clients.AllExcept(connId).onNewUserConnected(newUser);
            }
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                Clients.All.onUserDisconnected(item);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}