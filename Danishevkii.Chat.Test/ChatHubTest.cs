﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Dynamic;
using Danishevskii.Chat.Hubs;
using Danishevskii.Chat.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Owin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Danishevkii.Chat.Test
{
    [TestClass]
    public class ChatHubTest
    {

        [TestMethod]
        public void ChatHub_SendMethod_CorrectNameAdnMessage_Success()
        {
            bool sendCalled = false;
            var hub = new ChatHub();
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            hub.Clients = mockClients.Object;
            dynamic all = new ExpandoObject();
            all.addMessage = new Action<ChatMessage>((chat) => {
                sendCalled = true;
            });
            mockClients.Setup(m => m.All).Returns((ExpandoObject)all);
            hub.Send("TestUser", "TestMessage");
            Assert.IsTrue(sendCalled);
        }
    }
}
