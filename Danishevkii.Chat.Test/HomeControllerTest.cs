﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Danishevskii.Chat.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Danishevkii.Chat.Test
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void NameIntutTest()
        {
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.NameInput()as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CharBlockTest()
        {
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.ChatBlock("TestName") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
